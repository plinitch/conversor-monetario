#!/bin/bash

echo $0

if [ $1 -a $2 -a $3 ]; then

	conversor=`curl "https://api.exchangeratesapi.io/latest?base=$1&symbols=$2" | jq ".rates.$2"`

	python -c "print $3*$conversor"

else
	echo Por favor digite os códigos de moedas de origem, destino e valor a ser convertido
	exit 1
fi

