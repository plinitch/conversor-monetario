Name:           conversor-monetario
Version:        0.1
Release:        1
Summary:        Conversor monetário
BuildArch:	noarch

License:        GPLv3+
URL:            http://ftp.gnu.org/gnu/hello
Source0:        conversor-monetario-0.1.tar.gz
      
Requires: python
Requires: curl
Requires: jq

%description 
The "Conversor monetário" program, convert de valeu of one currency to another currency .

%prep
%autosetup

%build

%install
chmod 755 conversor-monetario.sh
mkdir -p %{buildroot}/usr/bin/
cp conversor-monetario.sh %{buildroot}/usr/bin/conversor

%files
/usr/bin/conversor
%doc README.md
%license LICENSE

%changelog
* Tue Aug 21 2018 Pablo Linitch <plinitch@gmail.com> 0.1-1
- Initial Version.

